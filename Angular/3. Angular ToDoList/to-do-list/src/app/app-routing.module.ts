import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounterComponent } from './counter/counter.component';
import { GalleryComponent } from './gallery/gallery.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { ErrorComponent } from './error/error.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'gallery', pathMatch: 'full' },
  { path: 'counter', component: CounterComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'calculator', component: CalculatorComponent },
  { path: 'todolist', component: ToDoListComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
