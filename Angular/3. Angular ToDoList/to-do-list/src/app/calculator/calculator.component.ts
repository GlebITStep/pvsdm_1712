import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  firstNumber = 6;
  secondNumber = 8;
  operation = '+';

  constructor() { }

  ngOnInit() {
  }

  get result(): number {
    switch (this.operation) {
      case '+':
        return this.firstNumber + this.secondNumber;
      case '-':
        return this.firstNumber - this.secondNumber;
      case '*':
        return this.firstNumber * this.secondNumber;
      case '/':
        return this.firstNumber / this.secondNumber;
      default:
        return 0;
    }
  }
}
