import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  images = [
    './../assets/images/1.jpg',
    './../assets/images/2.jpg',
    './../assets/images/3.jpg',
    './../assets/images/4.jpg',
    './../assets/images/5.jpg'
  ];
  currentImage = 0;

  constructor() { }

  ngOnInit() {
  }

  onPrevClick() {
    this.currentImage--;
  }

  onNextClick() {
    this.currentImage++;
  }
}
