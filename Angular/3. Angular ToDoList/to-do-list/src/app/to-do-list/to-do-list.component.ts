import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';
declare var $: any;

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss']
})
export class ToDoListComponent implements OnInit {
  tasks: Task[] = [];
  newTask: Task = new Task();
  selectedTask: Task;

  constructor() {
    // this.tasks.push(new Task('One'));
    // this.tasks.push(new Task('Two'));
    // this.tasks.push(new Task('Three'));

    let data = JSON.parse(localStorage.getItem('tasks'));
    if (data) {
      this.tasks = data; 
    }
  }

  ngOnInit() {
  }

  onTaskSubmit() {
    this.tasks.push(this.newTask);
    this.newTask = new Task();

    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  onDeleteClick(task: Task) {
    let index = this.tasks.indexOf(task);
    this.tasks.splice(index, 1);

    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  onCheckClick(task: Task) {
    task.done = !task.done;

    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  onTaskSelect(task: Task) {
    this.selectedTask = task;
    $('#myModal').modal('show');
  }

  onClearClick() {
    this.tasks = [];
    localStorage.clear();
  }
}
