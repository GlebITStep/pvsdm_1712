"use strict";
//number string boolean any
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// let age: number = 0;
// age = 'Gleb';
// let data: any = 0;
// data = 'Gleb';
// function Test(name: string): number {
// 	console.log(`Hello, ${name}`);
// 	return 42;	
// }
// // Test(0);
// Test('Gleb');
// let numbers1: number[] = [1,2,3];
// let numbers2: Array<number> = [1,2,3];
// enum Color {Red, Green, Blue};
// let color = Color.Blue; 
// function Test<T>(data: T): T {
// 	console.log(data);
// 	return data;
// }
// Test(5);
// Test('Gleb');
// Test(true);
var Person = /** @class */ (function () {
    function Person(name, age) {
        if (name === void 0) { name = 'Empty'; }
        if (age === void 0) { age = 0; }
        this.name = name;
        this.age = age;
    }
    Person.prototype.sayHello = function () {
        console.log("Hello from " + this.name);
    };
    return Person;
}());
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student(name, age, average) {
        if (name === void 0) { name = 'Empty'; }
        if (age === void 0) { age = 0; }
        if (average === void 0) { average = 0; }
        var _this = _super.call(this, name, age) || this;
        _this.average = average;
        return _this;
    }
    return Student;
}(Person));
var p = new Person('Gleb', 25);
var p2 = new Person();
// p.name = 'Gleb';
// p.age = 25;
console.log(p);
console.log(p2);
p.sayHello();
