//number string boolean any

// let age: number = 0;
// age = 'Gleb';
// let data: any = 0;
// data = 'Gleb';

// function Test(name: string): number {
// 	console.log(`Hello, ${name}`);
// 	return 42;	
// }
// // Test(0);
// Test('Gleb');

// let numbers1: number[] = [1,2,3];
// let numbers2: Array<number> = [1,2,3];

// enum Color {Red, Green, Blue};
// let color = Color.Blue; 

// function Test<T>(data: T): T {
// 	console.log(data);
// 	return data;
// }
// Test(5);
// Test('Gleb');
// Test(true);



class Person {
	name: string;
	age: number;
	
	constructor(name: string = 'Empty', age: number = 0) {
		this.name = name;
		this.age = age;
	}

	sayHello() {
		console.log(`Hello from ${this.name}`);	
	}
}

class Student extends Person {
	average: number;

	constructor(
	name: string = 'Empty', 
	age: number = 0, 
	average: number = 0) {
		super(name, age);
		this.average = average;
	}
}

let p: Person = new Person('Gleb', 25);
let p2: Person = new Person();
// p.name = 'Gleb';
// p.age = 25;
console.log(p);
console.log(p2);
p.sayHello();
