import { Component } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss']
})
export class CalcComponent {
  x: number = 0;
  y: number = 0;

  changeX(num: number) {
    this.x = num;
  }

  changeY(num: number) {
    this.y = num;
  }
}
