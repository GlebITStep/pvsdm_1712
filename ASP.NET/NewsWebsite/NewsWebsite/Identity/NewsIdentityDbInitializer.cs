﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Identity
{
    public class NewsIdentityDbInitializer
    {
        public static async Task SeedUsers(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var userManager = serviceScope
                    .ServiceProvider
                    .GetRequiredService<UserManager<AppUser>>();

                var roleManager = serviceScope
                    .ServiceProvider
                    .GetRequiredService<RoleManager<IdentityRole>>();

                if (!await roleManager.RoleExistsAsync("Admin"))
                    await roleManager.CreateAsync(new IdentityRole { Name = "Admin" });

                if (!await roleManager.RoleExistsAsync("User"))
                    await roleManager.CreateAsync(new IdentityRole { Name = "User" });

                if (await userManager.FindByNameAsync("admin@news.com") == null)
                {
                    var adminUser = new AppUser
                    {
                        UserName = "admin@news.com",
                        Email = "admin@news.com",
                        FirstName = "Gleb",
                        LastName = "Skripnikov"
                    };

                    await userManager.CreateAsync(adminUser, "admin");
                    await userManager.AddToRoleAsync(adminUser, "Admin");
                }
            }
        }
    }
}
