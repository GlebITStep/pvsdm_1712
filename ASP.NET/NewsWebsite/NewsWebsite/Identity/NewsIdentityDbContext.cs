﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Identity
{
    public class NewsIdentityDbContext : IdentityDbContext<AppUser>
    {
        public NewsIdentityDbContext(DbContextOptions<NewsIdentityDbContext> options) : base(options)
        {

        }
    }
}
