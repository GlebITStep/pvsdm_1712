﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NewsWebsite.Identity;
using NewsWebsite.Models;
using NewsWebsite.Services;

namespace NewsWebsite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var defaultConnection = Configuration.GetConnectionString("DefaultConnection");
            var identityConnection = Configuration.GetConnectionString("IdentityConnection");

            services.AddDbContext<NewsDbContext>(options => options.UseSqlServer(defaultConnection));
            services.AddDbContext<NewsIdentityDbContext>(options => options.UseSqlServer(identityConnection));

            services
                .AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<NewsIdentityDbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 5;

                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);

                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;

                options.User.RequireUniqueEmail = true;
                //options.User.AllowedUserNameCharacters = "Ə";
            });

            //services.ConfigureApplicationCookie(options =>
            //{
            //    options.AccessDeniedPath = "Error";
            //});

            services.AddAuthorization(options => 
            {
                options.AddPolicy("Admin", x => x.RequireRole("Admin"));
            });

            services.AddSingleton<IImageSaver, ImageSaver>();
            //services.AddScoped<IImageSaver, ImageSaver>();
            //services.AddTransient<IImageSaver, ImageSaver>();

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
            }
            //app.UseStatusCodePagesWithRedirects("/Home/Error/{0}");

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "admin",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            NewsDbInitializer.SeedData(app);
            NewsIdentityDbInitializer.SeedUsers(app).Wait();
        }
    }
}
