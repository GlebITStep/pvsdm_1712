﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewsWebsite.Models;
using NewsWebsite.ViewModels;

namespace NewsWebsite.Controllers
{
    public class HomeController : Controller
    {
        private readonly NewsDbContext context;

        public HomeController(NewsDbContext context)
        {
            this.context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await context.Posts.ToListAsync());
        }

        public IActionResult Post(int id)
        {
            return View(context.Posts.Include(p => p.Comments).FirstOrDefault(x => x.Id == id));
        }

        [HttpPost]
        public async Task<IActionResult> PostComment(string comment, int postId)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            context.Comments.Add(new Comment
            {
                Text = comment,
                PostId = postId,
                AppUserId = userId
            });
            await context.SaveChangesAsync();

            return RedirectToAction("Post", new { id = postId });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int errorId)
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
