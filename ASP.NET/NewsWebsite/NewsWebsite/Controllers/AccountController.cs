﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NewsWebsite.Identity;
using NewsWebsite.Services;
using NewsWebsite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IImageSaver imageSaver;

        public AccountController(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IImageSaver imageSaver)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.imageSaver = imageSaver;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            if (User?.Identity?.Name != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel data)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByNameAsync(data.Login);
                if (user != null)
                {
                    var result = await signInManager.PasswordSignInAsync(user, data.Password, true, false);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                }
            }
            ModelState.AddModelError("login_error", "Invalid login or password");
            return View(data);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(IFormFile picture, RegisterViewModel data)
        {
            if (ModelState.IsValid)
            {
                string imagePath = String.Empty;
                if (picture != null)
                {
                    imagePath = await imageSaver.SaveImageAsync(picture);
                }

                var user = new AppUser
                {
                    UserName = data.Login,
                    Email = data.Login,
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    ProfileImage = imagePath
                };

                var result = await userManager.CreateAsync(user, data.Password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "User");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(error.Code, error.Description);
                    }
                }
            }
            ModelState.AddModelError("register_error", "Invalid registration");
            return View(data);
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
