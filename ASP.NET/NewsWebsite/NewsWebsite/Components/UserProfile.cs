﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NewsWebsite.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Components
{
    public class UserProfile : ViewComponent
    {
        private readonly UserManager<AppUser> userManager;

        public UserProfile(UserManager<AppUser> userManager)
        {
            this.userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var username = User?.Identity?.Name;

            if (username != null)
            {
                var user = await userManager.FindByNameAsync(username);
                if (user != null)
                {
                    ViewBag.UserName = username;

                    if (user.ProfileImage != null)
                        ViewBag.ProfileImage = user.ProfileImage;
                    else
                        ViewBag.ProfileImage = "anon.png";
                }
            }

            return View();
        }
    }
}
