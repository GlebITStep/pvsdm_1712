﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Services
{
    public interface IImageSaver
    {
        Task<string> SaveImageAsync(IFormFile file);
    }
}
