﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NewsWebsite.Services
{
    class ImageSaver : IImageSaver
    {
        public async Task<string> SaveImageAsync(IFormFile file)
        {
            //cat.jpg -> asjfgasjkgasdgascat.jpg
            var filename = $"{Guid.NewGuid()}{file.FileName}";
            //c:\websites\wwwroot\images\profiles\asjfgasjkgasdgascat.jpg
            var fullname = $"{Directory.GetCurrentDirectory()}\\wwwroot\\images\\profiles\\{filename}";

            using (var fs = new FileStream(fullname, FileMode.Create))
            {
                await file.CopyToAsync(fs);
            }

            return filename;
        }
    }
}
