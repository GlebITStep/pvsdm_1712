﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Models
{
    public class Post
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Gde title?")]
        [MaxLength(200, ErrorMessage = "Your title is too long")]
        public string Title { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        [Required(ErrorMessage = "Gde content?")]
        public string Content { get; set; }

        public DateTime PublishDate { get; set; } = DateTime.Now;

        public IEnumerable<Comment> Comments { get; set; }

        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        public List<PostTag> PostTags { get; set; }
    }
}
