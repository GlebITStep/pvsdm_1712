﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string AppUserId { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public int PostId { get; set; }

        public Post Post { get; set; }
    }
}
