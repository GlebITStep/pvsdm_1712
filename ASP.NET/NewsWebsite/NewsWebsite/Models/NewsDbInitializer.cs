﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsWebsite.Models
{
    public class NewsDbInitializer
    {
        public static void SeedData(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                NewsDbContext context = serviceScope
                    .ServiceProvider
                    .GetRequiredService<NewsDbContext>();

                if (!context.Posts.Any())
                {
                    context.Posts.Add(new Post
                    {
                        Title = "Lorem ipsum dolor sit amet",
                        Image = "https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/02-cat-training-NationalGeographic_1484324.ngsversion.1526587209178.adapt.1900.1.jpg",
                        Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis elit vulputate, vehicula sapien in, porttitor tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum nunc felis, sit amet pellentesque ligula rhoncus in. Aliquam vel lobortis ex. Proin id felis est. Integer accumsan, tortor ac cursus rutrum, lacus diam volutpat metus, non suscipit ligula sapien et mi. Praesent maximus arcu quis erat ultricies euismod. Morbi euismod, mi vel commodo suscipit, erat massa suscipit justo, quis blandit enim neque at purus. Donec ac dapibus eros, euismod scelerisque leo. Duis semper ex vitae felis facilisis suscipit. Nulla tincidunt purus nisl, in faucibus felis sagittis nec. Aenean sit amet sem ut lectus tempus bibendum sit amet at urna. Integer a sollicitudin justo, et rutrum arcu. Sed ac arcu posuere, mattis tortor vestibulum, rhoncus diam. Aliquam bibendum tincidunt consectetur. Phasellus eleifend dui quis efficitur tincidunt.",
                        PublishDate = DateTime.Now
                    });
                    context.Posts.Add(new Post
                    {
                        Title = "Curabitur magna libero",
                        Image = "https://r.hswstatic.com/w_907/gif/tesla-cat.jpg",
                        Content = "Curabitur magna libero, euismod ac mi dictum, condimentum commodo dui. Mauris ut sollicitudin nunc. Nunc semper dapibus nunc, quis pulvinar elit gravida id. Maecenas eu odio quis lorem tincidunt euismod ac sed neque. Suspendisse ut ante a sem fringilla viverra. Curabitur mattis fermentum metus ac lobortis. In hac habitasse platea dictumst. Nam finibus, orci eget condimentum convallis, nunc eros lobortis urna, ut convallis est velit sed est. Duis gravida at quam sit amet feugiat. Nullam malesuada nibh vel enim ullamcorper, non vestibulum massa sodales. Maecenas imperdiet sed dui non ornare. Nunc fringilla ex ut malesuada consectetur.",
                        PublishDate = DateTime.Now
                    });
                    context.Posts.Add(new Post
                    {
                        Title = "Cras consequat elementum laoreet",
                        Image = "https://media.mnn.com/assets/images/2018/07/cat_eating_fancy_ice_cream.jpg.838x0_q80.jpg",
                        Content = "Cras consequat elementum laoreet. In rhoncus nunc ut cursus pellentesque. Donec vitae ipsum a libero sollicitudin dapibus. Phasellus sodales velit sapien, sed finibus erat luctus a. Ut blandit felis a nisl scelerisque euismod. Sed sit amet convallis nunc. Nulla vel dapibus turpis. Sed semper ante a mi fringilla hendrerit quis ut nisl. Sed sit amet rhoncus dui. Fusce sit amet malesuada neque, sed placerat nibh. Donec et ante id enim sollicitudin gravida. Cras in nisi velit. Vivamus efficitur a nulla eu maximus. Pellentesque sodales turpis et mi cursus condimentum. Sed eu nisl mauris. Integer mollis ligula urna, ut interdum justo cursus porta.",
                        PublishDate = DateTime.Now
                    });
                    context.Posts.Add(new Post
                    {
                        Title = "Vestibulum lacus velit",
                        Image = "https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg",
                        Content = "Vestibulum lacus velit, egestas non lectus vel, consequat luctus dolor. Nulla finibus eros est, eu convallis tortor sodales a. Aliquam diam nulla, sollicitudin nec finibus quis, tincidunt nec est. Phasellus accumsan varius augue. Maecenas efficitur, elit vel rhoncus porta, tellus quam cursus sem, nec blandit turpis erat quis nisi. Maecenas egestas dolor leo, vel sodales lectus luctus eget. Fusce fermentum congue varius. Aliquam sit amet facilisis nisi, quis maximus metus. Vestibulum ac nisl eu ipsum venenatis auctor. Nam ut augue ut tortor pharetra elementum id sit amet dolor. Fusce ut massa sit amet est ornare luctus. Quisque porttitor ac massa id auctor. Integer fermentum sodales interdum. Cras pharetra rutrum nisl nec fringilla. Fusce sollicitudin ornare venenatis.",
                        PublishDate = DateTime.Now
                    });
                    context.SaveChanges();
                }
            }
        }
    }
}
