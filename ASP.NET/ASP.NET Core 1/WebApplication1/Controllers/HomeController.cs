﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About(Person p)
        {
            //ViewData["Name"] = "Gleb";
            //ViewData["Age"] = 25;

            //ViewBag.Name = "Gleb";
            //ViewBag.Age = 25;

            var person = new Person { Name = p.Name, Age = p.Age };
            return View(person);
        }

        public IActionResult Error()
        {
            return View();
        }

        //public IActionResult About(string name, int age)
        //{
        //    //ViewData["Name"] = "Gleb";
        //    //ViewData["Age"] = 25;

        //    //ViewBag.Name = "Gleb";
        //    //ViewBag.Age = 25;

        //    var person = new Person { Name = name, Age = age };
        //    return View(person);
        //}

        public IActionResult String()
        {
            return Content("String");
        }

        public IActionResult Json()
        {
            return Json(new { Name = "Gleb", Age = 25 });
        }
    }
}
