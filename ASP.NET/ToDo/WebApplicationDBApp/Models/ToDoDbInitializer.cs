﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationDBApp.Models
{
    public static class ToDoDbInitializer
    {
        public static void SeedData(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                ToDoDbContext context = serviceScope
                    .ServiceProvider
                    .GetRequiredService<ToDoDbContext>();

                if (!context.ToDos.Any())
                {
                    context.ToDos.Add(new ToDo
                    {
                        Name = "One",
                        Description = "Description One",
                        IsDone = false
                    });
                    context.ToDos.Add(new ToDo
                    {
                        Name = "Two",
                        Description = "Description Two",
                        IsDone = false
                    });
                    context.ToDos.Add(new ToDo
                    {
                        Name = "Three",
                        Description = "Description Three",
                        IsDone = false
                    });
                    context.SaveChanges();
                }
            }
        }
    }
}
