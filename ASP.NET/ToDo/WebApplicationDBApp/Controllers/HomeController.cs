﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationDBApp.Models;

namespace WebApplicationDBApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ToDoDbContext context;

        public HomeController(ToDoDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            //ViewBag.Tasks = context.ToDos.ToList();
            return View(context.ToDos.Take(2).ToList());
        }

        public IActionResult Task(int id)
        {
            var task = context.ToDos.FirstOrDefault(x => x.Id == id);
            return View(task);
        }

        [HttpGet]
        public IActionResult AddTask()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddTask(ToDo task)
        {
            context.ToDos.Add(task);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteTask(int id)
        {
            var task = context.ToDos.FirstOrDefault(x => x.Id == id);

            if (task != null)
            {
                context.ToDos.Remove(task);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult ToggleTaskStatus(int id)
        {
            var task = context.ToDos.FirstOrDefault(x => x.Id == id);

            if (task != null)
            {
                task.IsDone = !task.IsDone;
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}