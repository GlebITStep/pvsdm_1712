//835492c0eab300994ec658dfb16ad305
//https://api.openweathermap.org/data/2.5/weather?q=Baku&appid=835492c0eab300994ec658dfb16ad305&units=metric

let url = 'https://api.openweathermap.org/data/2.5/weather';
let apiKey = '835492c0eab300994ec658dfb16ad305';
let xhr = new XMLHttpRequest();

function searchWeather() {
    let cityName = document.forms.citySearch.city.value;

    xhr.open('GET', `${url}?q=${cityName}&appid=${apiKey}&units=metric`);
    xhr.send();
    xhr.onloadend = showWeather;

    event.preventDefault();
}

function showWeather() {
    let data = JSON.parse(xhr.response); 
    console.log(data);
    
    let html = document.querySelector('#weather-template').innerHTML;
    let template = Handlebars.compile(html);
    let result = template(data);
    document.querySelector('#result').innerHTML += result;
}

function deleteWeather() {
    event.target.parentElement.remove();
}

function keyDown() {
    let text = document.forms.citySearch.city.value;
    if (text != "") {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `https://restcountries.eu/rest/v2/name/${text}?fields=name;`);
        xhr.send();
        xhr.onloadend = function() {
            let list = document.querySelector('#cities');
            list.innerHTML = "";
            let data = JSON.parse(xhr.response);
            for (let i = 0; i < 10; i++) {
                let option = document.createElement('option');
                option.value = data[i].name;
                list.appendChild(option);
                
            }
            // for (const item of data) {
            //     let option = document.createElement('option');
            //     option.value = item.name;
            //     list.appendChild(option);
            // }
        }
    }

}