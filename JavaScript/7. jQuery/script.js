// var elem1 = document.querySelector('.box');
// var elem2 = $('.box');
// console.log(elem1);
// console.log(elem2);

// //JS
// var elems1 = document.querySelectorAll('.box > p');
// for (const item of elems1) {
//     item.textContent = "JS";
// }
// console.log(elems1);

// //jQuery
// var elems2 = $('.box > p');
// elems2.text('jQuery');
// console.log(elems2);

// let count = 1;
// for (const item of elems2) {
//     item.textContent = count++;
// }

// let count = 1;
// elems2.each(function() {
//     $(this).text(count++);
// });



// document.addEventListener('DOMContentLoaded', function() {
//     console.log('Loaded!'); 
// });

// $(document).ready(function() {
//     console.log('Ready!');  
// });

// $(function() {
//     console.log('Ready2!');  
// });



$(function() {

    // $('.box').click(function() {
    //     $(this).hide(500);   
    // });



    // $('h1').click(function(event) {
    //     // $(this).next().toggle(500); //hide() show()

    //     // $(this).next().slideToggle(500); //slideUp() slideDown()

    //     // $(this).next().fadeToggle(500);

    //     // $(this).next().animate({
    //     //     height: '100px', 
    //     //     width: '200px',
    //     //     backgroundColor: 'green'
    //     // }, 1000);

    //     $(this).next()
    //         .animate({height: '100px'}, 1000) 
    //         .animate({width: '200px'}, 1000) 
    //         .animate({backgroundColor: 'green'}, 1000, function() {
    //             console.log('Done!'); 
    //         });        
    // });



    // let text = $('h1').text();
    // console.log(text);


    // $('h1').each(function (index) {
    //     if (index == 1) {
    //         console.log($(this).text());  
    //     }
    // });
    

    //console.log($('h1').eq(1).text());


    //$('h1').text('Hello!');


    //console.log($('.box').eq(1).html());


    // $('.box').click(function() {
    //     $(this).html('<button>Test</button>');
    // }) 
    
    
    // console.log($('#name').val());
    // console.log($('#select').val()); 


    // $('#name').val('Vugar');
    // $('#select').val(3);


    // $('.box').click(function() {
    //     // $(this).toggleClass('green'); //addClass() removeClass() toggleClass()

    //     // let color = $(this).css("background-color");
    //     // console.log(color);
        
    //     //$(this).css("background-color", "blue");

    //     $(this).css({
    //         height: '100px', 
    //         width: '200px',
    //         backgroundColor: 'green'
    //     });
    // }) 


    // $('a').click(function(event) {
    //     let url = $(this).attr('href');
    //     console.log(url); 
    //     event.preventDefault();
    // });


    // $('a').attr('href', 'https://www.itstep.az');


    // $('h1').click(function() {
    //     $(this).append('<button>Hello!</button>');
    //     $(this).prepend('<button>Hello!</button>');
    //     $(this).after('<button>Hello!</button>');
    //     $(this).before('<button>Hello!</button>');
    // });


    // $('p').click(function() {
    //     // $(this).parent().css('border', '2px solid black');
    //     // $(this).parents().css('border', '2px solid black');$(this).parents().css('border', '2px solid black');
    //     // $(this).parentsUntil('html').css('border', '2px solid black');
    // });


    // $('.box').click(function() {
    //     // $(this).children().text('Test');
    //     // $(this).children().eq(1).text('Test');
    //     // $(this).find('.three').text('Test');
    // });
});