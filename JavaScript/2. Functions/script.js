


// console.log(this);
// let obj = {
//     name: 'Gleb',
//     age: 24,
//     say: function() {
//         console.log(this);
//         //console.log(`My name is ${this.name}`);  
//     }
// }
// obj.say();

// function Person(name, age) {
//     obj = {};
//     obj.name = name;
//     obj.age = age;
//     return obj;
// }

// let p1 = Person('Gleb', 24);
// console.log(p1);


// function Person(name, age) {
//     this.name = name;
//     this.age = age;

//     this.say = function() {
//         console.log(`My name is ${this.name}`);  
//     }
// }

// let p1 = new Person('Gleb', 24);
// console.log(p1);
// p1.say();


class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    say() {
        console.log(`My name is ${this.name}`); 
    }
}

let p1 = new Person('Gleb', 24);
console.log(p1);
p1.say();

