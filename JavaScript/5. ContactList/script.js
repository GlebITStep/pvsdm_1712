function deleteContact() {
    event.target.parentElement.remove();
}

function addContact() {
    let html = document.querySelector('#contact-template').innerHTML;
    let template = Handlebars.compile(html);
    let data = {
        name: document.forms.contactForm.name.value,
        surname: document.forms.contactForm.surname.value,
        phone: document.forms.contactForm.phone.value,
        email: document.forms.contactForm.email.value,
    }
    let result = template(data);
    document.querySelector('#contacts').innerHTML += result;
    
    event.preventDefault();
}

// function addContact() {
//     let container = document.createElement('div');
//     container.className = 'contact';

//     let pName = document.createElement('p');
//     pName.textContent = document.forms.contactForm.name.value;
//     container.appendChild(pName);

//     let pSurname = document.createElement('p');
//     pSurname.textContent = document.forms.contactForm.surname.value;
//     container.appendChild(pSurname);

//     let pPhone = document.createElement('p');
//     pPhone.textContent = document.forms.contactForm.phone.value;
//     container.appendChild(pPhone);

//     let pEmail = document.createElement('p');
//     pEmail.textContent = document.forms.contactForm.email.value;
//     container.appendChild(pEmail);

//     let deleteButton = document.createElement('button');
//     deleteButton.textContent = '💀';
//     container.appendChild(deleteButton);

//     deleteButton.addEventListener('click', function() {
//         // console.log(event.target.parentElement); 
//         event.target.parentElement.remove();
//     });

//     document.querySelector('#contacts').appendChild(container);
//     document.forms.contactForm.reset();


//     event.preventDefault();
// }



// function addContact() {
//     let name = document.getElementById("name").value;
//     console.log(`Name: ${name}`); 
//     let surname = document.querySelector("#surname").value;
//     console.log(`Surname: ${surname}`); 
//     let phone = document.getElementsByName("phone")[0].value;
//     console.log(`Phone: ${phone}`); 
//     let email = document.forms.contactForm.email.value;
//     console.log(`Email: ${email}`); 
//     event.preventDefault();
// }