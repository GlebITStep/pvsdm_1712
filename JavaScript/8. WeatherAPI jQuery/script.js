let url = 'https://api.openweathermap.org/data/2.5/weather';
let apiKey = '835492c0eab300994ec658dfb16ad305';

$(function() {

    $('#result').sortable();


    $('#search').submit(function(event) {
        let city = $('#city').val(); 

        let data = { 
            q: city,
            appid: apiKey,
            units: 'metric'
        };

        $.get(url, data, function(data) {
            console.log(data); 

            let html = $('#weather-template').html();
            let template = Handlebars.compile(html);
            let result = template(data);
            $('#result').append(result);
            $('#result > div:last-of-type > button').click(function() {
                $(this).parent().remove();
            }); 
            $('#result > div:last-of-type').resizable();
        })

        event.preventDefault(); 
    });

});