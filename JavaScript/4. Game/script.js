let player = null;

document.addEventListener("DOMContentLoaded", function() {
    player = document.querySelector("#player");
    for (let i = 0; i < 10; i++) {
        addFood();
    }

    player.addEventListener("transitionend", function() {
        checkCollision();
    })
});

function checkCollision() {
    let top = parseInt(getComputedStyle(player).top);
    let left = parseInt(getComputedStyle(player).left);
    let height = parseInt(getComputedStyle(player).height);
    let width = parseInt(getComputedStyle(player).width);

    var foods = document.querySelectorAll(".food");
    for (const food of foods) {
        let foodTop = parseInt(getComputedStyle(food).top);
        let foodLeft = parseInt(getComputedStyle(food).left);
        let foodHeight = parseInt(getComputedStyle(food).height);
        let foodWidth = parseInt(getComputedStyle(food).width);
        let foodY = foodTop + foodWidth / 2;
        let foodX = foodLeft + foodHeight / 2;
        
        if((foodX >= left && foodX <= left + width) 
        && (foodY >= top && foodY <= top + height)) {
            food.remove();           
        }  
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function addFood() {
    let width = document.body.clientWidth;
    let height = document.body.clientHeight;
    let elem = document.createElement('div');
    elem.className = "food";
    elem.style.top = getRandomInt(0, height - 25) + "px";
    elem.style.left = getRandomInt(0, width - 25) + "px";
    document.body.appendChild(elem); 
}

function movePlayer() {
    let speed = 50;
    let top = parseInt(getComputedStyle(player).top);
    let left = parseInt(getComputedStyle(player).left);
    switch (event.key) {
        case "ArrowUp":
            player.style.top = top - speed + "px";
            break;
        case "ArrowDown":
            player.style.top = top + speed + "px";
            break;
        case "ArrowLeft":
            player.style.left = left - speed + "px";
            break;
        case "ArrowRight":
            player.style.left = left + speed + "px";
            break;
        default:
            break;
    }
}