﻿using GalaSoft.MvvmLight;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp13
{
    public partial class MainWindow : Window
    {
        AppViewModel data = new AppViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = data;
        }

        static public int x = 0;

        //copy x to CPU reg
        //x++
        //copy x to RAM

        object locker = new object();

        void Func(dynamic obj)
        {
            int iters = obj.Iters;
            for (int i = 0; i < iters; i++)
            {
                //Interlocked.Increment(ref x);
                lock (locker)
                {
                    x++;
                }
            }
            data.Output = $"{x.ToString()} {obj.Text}";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(Func, new { Iters = 1000000, Text = "Test" });
            ThreadPool.QueueUserWorkItem(Func, new { Iters = 1000000, Text = "Test" });
            ThreadPool.QueueUserWorkItem(Func, new { Iters = 1000000, Text = "Test" });
            ThreadPool.QueueUserWorkItem(Func, new { Iters = 1000000, Text = "Test" });


            //ThreadPool.QueueUserWorkItem(
            //    (dynamic obj) =>
            //    {
            //        int iters = obj.Iters;
            //        int x = 0;
            //        for (int i = 0; i < iters; i++)
            //        {
            //            x++;
            //        }
            //        data.Output = $"{x.ToString()} {obj.Text}";
            //    }, new { Iters = 2000000000, Text = "Test" });

            //Thread thread = new Thread(Func);
            //thread.IsBackground = true;
            //thread.Start(new { Iters = 2000000000, Text = "Test" });
        }
    }

    class Params
    {
        public int Iters { get; set; }
        public string Text { get; set; }
    }

    class AppViewModel : ObservableObject
    {
        private string output;
        public string Output { get => output; set => Set(ref output, value); }
    }
}
