﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp13
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        void Func(dynamic obj)
        {
            //int iters = ((ValueTuple<int, string>)iterations).Item1;
            //string str = ((ValueTuple<int, string>)iterations).Item2;
            int iters = obj.Iters;
            int x = 0;
            for (int i = 0; i < iters; i++)
            {
                x++;
                Dispatcher.Invoke(() => Txt.Text = $"{x.ToString()} {obj.Text}");
            }
            //MessageBox.Show($"{x.ToString()} {obj.Text}");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(Func);
            //thread.Start((2000000000, "Test"));
            thread.IsBackground = true;
            thread.Start(new { Iters = 200000000, Text = "Test" });
        }
    }

    class Params
    {
        public int Iters { get; set; }
        public string Text { get; set; }
    }
}
