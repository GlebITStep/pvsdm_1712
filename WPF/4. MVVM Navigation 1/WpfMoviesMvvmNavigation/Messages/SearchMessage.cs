﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMoviesMvvmNavigation.Messages
{
    class SearchMessage
    {
        public string Text { get; set; }
    }
}
