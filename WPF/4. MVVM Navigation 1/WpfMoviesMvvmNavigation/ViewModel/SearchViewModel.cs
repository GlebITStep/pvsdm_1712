﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMoviesMvvmNavigation.Messages;

namespace WpfMoviesMvvmNavigation.ViewModel
{
    class SearchViewModel : ViewModelBase
    {
        private string searchText;
        public string SearchText { get => searchText; set => Set(ref searchText, value); }

        private RelayCommand sendCommand;
        public RelayCommand SendCommand
        {
            get => sendCommand ?? (sendCommand = new RelayCommand(
                () =>
                {
                    Messenger.Default.Send(new SearchMessage { Text = SearchText });
                    Messenger.Default.Send(new NavigationMessage { Name = "MovieDetails" });
                }
            ));
        }
    }
}
