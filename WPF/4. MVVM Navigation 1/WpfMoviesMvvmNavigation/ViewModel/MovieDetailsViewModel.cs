﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMoviesMvvmNavigation.Messages;

namespace WpfMoviesMvvmNavigation.ViewModel
{
    class MovieDetailsViewModel : ViewModelBase
    {
        private string result;
        public string Result { get => result; set => Set(ref result, value); }

        public MovieDetailsViewModel()
        {
            Messenger.Default.Register<SearchMessage>(this, param =>
            {
                Result = param.Text;
            });
        }

        private RelayCommand backCommand;
        public RelayCommand BackCommand
        {
            get => backCommand ?? (backCommand = new RelayCommand(
                () => Messenger.Default.Send(new NavigationMessage { Name = "Search" })
            ));
        }
    }
}
