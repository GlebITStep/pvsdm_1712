﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMoviesMvvmNavigation.Messages;

namespace WpfMoviesMvvmNavigation.ViewModel
{
    class AppViewModel : ViewModelBase
    {
        private ViewModelBase currentPage;
        public ViewModelBase CurrentPage { get => currentPage; set => Set(ref currentPage, value); }

        Dictionary<string, ViewModelBase> pages = new Dictionary<string, ViewModelBase>();

        public AppViewModel()
        {
            pages.Add("Search", new SearchViewModel());
            pages.Add("MovieDetails", new MovieDetailsViewModel());

            CurrentPage = pages["Search"];

            Messenger.Default.Register<NavigationMessage>(this, param =>
            {
                CurrentPage = pages[param.Name];
            });
        }

        //private RelayCommand<string> goToCommand;
        //public RelayCommand<string> GoToCommand
        //{
        //    get => goToCommand ?? (goToCommand = new RelayCommand<string>(
        //        param => CurrentPage = pages[param]
        //    ));
        //}
    }
}
