﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace WpfToDoList
{
    class AppViewModel : ObservableObject
    {
        private ObservableCollection<ToDo> tasks = new ObservableCollection<ToDo>();
        public ObservableCollection<ToDo> Tasks
        {
            get => tasks;
            set => Set(ref tasks, value);
        }

        private string input = "";
        public string Input
        {
            get => input;
            set => Set(ref input, value);
        }

        //private ToDo selected;
        //public ToDo Selected
        //{
        //    get => selected;
        //    set => Set(ref selected, value);
        //}

        public AppViewModel()
        {
            Tasks.Add(new ToDo { Title = "One", Done = false });
            Tasks.Add(new ToDo { Title = "Two", Done = true });
            Tasks.Add(new ToDo { Title = "Three", Done = false });
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get => addCommand ?? (addCommand = new RelayCommand(
                param =>
                {
                    Tasks.Add(new ToDo { Title = Input, Done = false });
                    Input = "";
                },
                param =>
                {
                    Regex regex = new Regex("^[a-z, ]*$");
                    return regex.IsMatch(Input) && !String.IsNullOrWhiteSpace(Input);      
                }
                //param => !String.IsNullOrWhiteSpace(Input)
            ));
        }


        private RelayCommand checkCommand;
        public RelayCommand CheckCommand
        {
            get => checkCommand ?? (checkCommand = new RelayCommand(
                param =>
                {
                    var todo = param as ToDo;
                    todo.Done = !todo.Done;
                }
            ));
        }
    }
}
