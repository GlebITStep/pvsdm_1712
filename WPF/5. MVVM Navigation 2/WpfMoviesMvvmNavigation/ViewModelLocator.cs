﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMoviesMvvmNavigation.ViewModel;
using WpfMoviesMvvmNavigation.Navigation;

namespace WpfMoviesMvvmNavigation
{
    class ViewModelLocator
    {
        public AppViewModel AppViewModel { get; set; }
        public SearchViewModel SearchViewModel { get; set; }
        public MovieDetailsViewModel MovieDetailsViewModel { get; set; }

        public NavigationService Navigation { get; set; }

        public ViewModelLocator()
        {
            Navigation = new NavigationService();

            AppViewModel = new AppViewModel();
            SearchViewModel = new SearchViewModel(Navigation);
            MovieDetailsViewModel = new MovieDetailsViewModel(Navigation);

            Navigation.RegisterPage("Search", SearchViewModel);
            Navigation.RegisterPage("MovieDetails", MovieDetailsViewModel);

            Navigation.GoTo("MovieDetails");
        }
    }
}
