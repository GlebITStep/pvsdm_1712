﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMoviesMvvmNavigation.Messages;
using WpfMoviesMvvmNavigation.Navigation;

namespace WpfMoviesMvvmNavigation.ViewModel
{
    class MovieDetailsViewModel : ViewModelBase
    {
        private string result;
        public string Result { get => result; set => Set(ref result, value); }

        private readonly NavigationService navigationService;

        public MovieDetailsViewModel(NavigationService navigationService)
        {
            this.navigationService = navigationService;

            Messenger.Default.Register<SearchMessage>(this, param =>
            {
                Result = param.Text;
            });
        }

        private RelayCommand backCommand;
        public RelayCommand BackCommand
        {
            get => backCommand ?? (backCommand = new RelayCommand(
                () => navigationService.GoTo("Search")
            ));
        }
    }
}
