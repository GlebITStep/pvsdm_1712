﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMoviesMvvmNavigation.Navigation
{
    public class NavigationService
    {
        Dictionary<string, ViewModelBase> pages = new Dictionary<string, ViewModelBase>();

        public void GoTo(string name)
        {
            Messenger.Default.Send(pages[name]);
        }

        public void RegisterPage(string name, ViewModelBase vm)
        {
            pages.Add(name, vm);
        }
    }
}
