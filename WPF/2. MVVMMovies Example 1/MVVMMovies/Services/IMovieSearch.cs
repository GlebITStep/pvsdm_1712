﻿using MVVMMovies.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMMovies.Services
{
    interface IMovieSearch
    {
        IEnumerable<Movie> Search(string title);
        Movie FullInfo(string imdbId);
    }
}
