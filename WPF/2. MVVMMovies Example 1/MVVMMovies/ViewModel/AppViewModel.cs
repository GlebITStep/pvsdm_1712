﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MVVMMovies.Model;
using MVVMMovies.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MVVMMovies.ViewModel
{
    class AppViewModel : ViewModelBase
    {
        private string input = "Terminator";
        public string Input { get => input; set => Set(ref input, value); }

        private Movie selected;
        public Movie Selected { get => selected; set => Set(ref selected, value); }

        private Movie fullMovie;
        public Movie FullMovie { get => fullMovie; set => Set(ref fullMovie, value); }

        private ObservableCollection<Movie> list = new ObservableCollection<Movie>();
        public ObservableCollection<Movie> List { get => list; set => Set(ref list, value); }

        private readonly IMovieSearch movieSearch;

        public AppViewModel(IMovieSearch movieSearch)
        {
            this.movieSearch = movieSearch;
        }

        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get => searchCommand ?? (searchCommand = new RelayCommand(
                () => List = new ObservableCollection<Movie>(movieSearch.Search(Input))
            ));
        }

        private RelayCommand fullInfoCommand;
        public RelayCommand FullInfoCommand
        {
            get => fullInfoCommand ?? (fullInfoCommand = new RelayCommand(
                () => FullMovie = movieSearch.FullInfo(Selected.Id)
            ));
        }
    }
}
